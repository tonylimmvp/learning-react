import React, { useState } from 'react';

// This CounterExample function teaches us how to control or change state with react.
function CounterExample(){
    //Creating a constant array via hook
    //The setCount is a function to change the count value. You can rename this to with another name.
    //Setting the default value as 0.
    const [count, setCount] = useState(0)
    
    return(
        <div>
            <h1>
                {count}
            </h1>

            {/* This is a onClick function that will add count by 1 */}
            <h1 onClick={() => setCount(count + 1)}>
                Plus
            </h1>

            {/* This is a onClick function that will minus count by 1 */}
            <h1 onClick={() => setCount(count - 1)}>
                Minus
            </h1>
        </div>

    )
}

export default CounterExample