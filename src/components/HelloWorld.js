import React from 'react';

// Functional Component Creation Called HelloWorld
/**  function HelloWorld(props){
//     return (
//         <h1>Hello {props.name}</h1>
//     )
} */

// Class Component Creation Called HelloWorld
// Component should always render something to be returned. 
// Notice here that render is a function. And since component is a class object, we have to use this.props to distinguish which classes to get from.
class HelloWorld extends React.Component{
    render(){
        return (
            <h1> Hello {this.props.name}</h1>
        )
    }
}

export default HelloWorld