import React from 'react';
import logo from './logo.svg';
import './App.css';
import HelloWorld from './components/HelloWorld'
import CounterExample from './components/CounterExample'
import Header from './components/Header'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* This name definition is called "props" which is used in the function/component (Hello World) arguments */}
        {/* Props is used to carry dynamic data between components or functional components */}
        <HelloWorld name="Tony Lim"/>

        <Header />

        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
